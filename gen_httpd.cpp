#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <variant>
#include <vector>

struct AcmeConfig {
  bool setup;
  std::string key, certificate, fullchain;
  std::optional<std::vector<std::string>> alt_names;
};

struct Certificate {
  std::string key, cert;
};

struct Domain {
  std::string domain;
  std::string document_root;
  std::optional<AcmeConfig> acme;
  std::optional<std::variant<std::reference_wrapper<AcmeConfig>, Certificate>>
      tls;
  std::optional<std::string> redirect;

  std::optional<Certificate> get_cert() {
    if (!tls)
      return std::nullopt;
    Certificate cert;
    try {
      AcmeConfig acme =
          std::get<std::reference_wrapper<AcmeConfig>>(tls.value()).get();
      cert.key = acme.key;
      cert.cert = acme.fullchain;
    } catch (const std::bad_variant_access &ex) {
      return (std::get<Certificate>(tls.value()));
    }
    return cert;
  }

  void get_httpd_conf(std::ostream &out = std::cout) {
    out << "server \"" << domain << "\" {\n";

    if (tls) {
      Certificate cert =
          get_cert()
              .value(); /* should always exist since we're within TLS context */

      if (acme && !acme.value().setup) {
        out << "  listen on * port 80\n"
            << "  root \"" << document_root << "\"\n"
            << "  location \"/.well-known/acme-challenge/*\" {\n"
            << "    root \"/acme\"\n"
            << "    request strip 2\n"
            << "  }\n";
      } else {
        out << "  listen on * tls port 443\n";
        if (redirect) {
          out << "  block return 301 \"" << redirect.value() << "\"\n";
        }
        out << "  root \"" << document_root << "\"\n"
            << "  tls {\n"
            << "    certificate \"" << cert.cert << "\"\n"
            << "    key \"" << cert.key << "\"\n"
            << "  }\n"
            << "  location \"/.well-known/acme-challenge/*\" {\n"
            << "    root \"/acme\"\n"
            << "    request strip 2\n"
            << "  }\n}\n"
            << "server \"" << domain << "\" {\n"
            << "  listen on * port 80\n"
            << "  block return 301 \"https://" << domain << "$REQUEST_URI\"\n";
      }
    } else {
      out << "  listen on * port 80\n"
          << "  root \"" << document_root << "\"\n";
    }

    out << "}\n" << std::endl;
  }

  static void get_acme_preamble(std::ostream &out = std::cout) {
    out << "authority letsencrypt {\n"
        << "  api url \"https://acme-v02.api.letsencrypt.org/directory\"\n"
        << "  account key \"/etc/ssl/private/letsencrypt.key\"\n"
        << "}\n"
        << std::endl;
  }

  void get_acme_conf(std::ostream &out = std::cout) {
    if (!this->acme)
      return;
    AcmeConfig acme = this->acme.value();

    out << "domain " << domain << " {\n";
    if (acme.alt_names) {
      out << "  alternative names { ";
      std::for_each(acme.alt_names.value().begin(),
                    acme.alt_names.value().end(),
                    [&out](const std::string n) { out << n << " "; });
      out << "}\n";
    }
    out << "  domain key \"" << acme.key << "\"\n"
        << "  domain certificate \"" << acme.certificate << "\"\n"
        << "  domain full chain certificate \"" << acme.fullchain << "\"\n"
        << "  sign with letsencrypt\n"
        << "}\n"
        << std::endl;
  }
};

namespace Action {
void execute(std::vector<Domain> &dom) {}
}; // namespace Action

int main() {
  Domain d = Domain{};

  std::vector<std::string> alt_names{"www.sergal.lol", "git.sergal.lol"};
  Certificate cert{"/etc/ssl/sergal.lol.pem",
                   "/etc/ssl/private/sergal.lol.key"};
  d.domain = "sergal.lol";
  d.document_root = "/htdocs/sergal.lol";
  d.acme = AcmeConfig{true, "/etc/ssl/private/sergal.lol.key",
                      "/etc/ssl/sergal.lol.crt", "/etc/ssl/sergal.lol.pem",
                      alt_names};
  d.tls = cert;

  d.get_httpd_conf();
  std::cout << "--------\n";
  Domain::get_acme_preamble();
  d.get_acme_conf();

  return 0;
}
